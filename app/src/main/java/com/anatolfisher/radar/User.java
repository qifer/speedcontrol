package com.anatolfisher.radar;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String lastname;
    public String username;
    public String middlename;
    public String series;
    public String number;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String lastname, String username, String middlename, String series, String number) {
        this.lastname = lastname;
        this.username = username;
        this.middlename = middlename;
        this.series = series;
        this.number = number;
    }

}