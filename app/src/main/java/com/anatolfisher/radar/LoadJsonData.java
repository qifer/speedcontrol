package com.anatolfisher.radar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoadJsonData extends AsyncTask<Void, Void, String> {

	String GuidControl;
	String Param1;
	String Param2;
	String Param3;
	Context context;

	private SharedPreferences preferences;
	private String LOG_TAG = "##LoadJsonData";
	public LoadJsonData(Context context) {
		super();
		preferences = PreferenceManager.getDefaultSharedPreferences(context);

		this.GuidControl = preferences.getString("GuidControl", "-");
		this.Param1 = preferences.getString("Param1", "-");
		this.Param2 = preferences.getString("Param2", "-");
		this.Param3 = preferences.getString("Param3", "-");
		this.context = context;

		/*this.GuidControl = "2091";
		this.Param1 = "Татарчук Анатолий Иванович";
		this.Param2 = "МАА";
		this.Param3 = "2090479";*/


		//Log.e(LOG_TAG, "this.link ");
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		Log.e(LOG_TAG, "!!! onPreExecute: ");
	}

	@Override
	protected String doInBackground(Void... voids) {
		final String url = "http://mvd.gov.by/Ajax.asmx/GetExt";
		final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

		OkHttpClient client = new OkHttpClient();
		//String json = "{\"GuidControl\":2091,\"Param1\":\"Татарчук Анатолий Иванович\",\"Param2\":\"МАА\",\"Param3\":\"2090479\"}";


		/*HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
		urlBuilder.addQueryParameter("GuidControl", GuidControl);
		urlBuilder.addQueryParameter("Param1", "\"" + Param1 + "\"");
		urlBuilder.addQueryParameter("Param2", "\"" + Param2 + "\"");
		urlBuilder.addQueryParameter("Param3", "\"" + Param3 + "\"");
		String url_ = urlBuilder.build().toString();*/

		JSONObject _json = new JSONObject();
		try {
			_json.put("GuidControl", GuidControl);
			_json.put("Param1", Param1);
			_json.put("Param2", Param2);
			_json.put("Param3", Param3);

			//String json_ = "{\"GuidControl\":2091,\"Param1\":\"Татарчук Анатолий Иванович\",\"Param2\":\"МАА\",\"Param3\":\"2090479\"}";
			String json = _json.toString();

			RequestBody body = RequestBody.create(JSON, json);
			Request request = new Request.Builder()
					.url(url)
					.post(body)
					.build();
			try {
				Response response = client.newCall(request).execute();
				assert response.body() != null;
				String str = response.body().string();

				str = str.replace("\\u003C", "<")
						.replace("\\u003c", "<")
						.replace("\\u003E", ">")
						.replace("\\u003e", ">")
						.replace("\\r\\n", "");
						//.replace("\\\"", "\'")
						//.replace("\"", "");

				if (str.charAt(0) == '"') {
					str = str.substring(1, str.length()-1);
				}

				//System.out.println(str);

				//String result = java.net.URLDecoder.decode(str, "UTF-8");
				Log.e(LOG_TAG, str);
				return str;
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}


		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		((LoginActivity) context).parseResponse(result);
		/*if (delegate != null) {
			delegate.parseJsonResponse(RESULT, num);
		}*/
		Log.e(LOG_TAG, "result: " + result);
	}
}
