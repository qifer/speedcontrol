package com.anatolfisher.radar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.DatabaseMetaData;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private SharedPreferences preferences;

    String LOG_TAG = "##LoginActivity";

    private View mProgressView;

    private View mLoginFormView;
    private WebView webView;

    private EditText mLastNameView;
    private EditText mNameView;
    private EditText mMiddleNameView;
    private EditText mSeriesView;
    private EditText mNumberView;

    private Button mReloginButton;

    private DatabaseReference mDatabase;

    private String androidId;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_2);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase = FirebaseDatabase.getInstance().getReference("users");

        /*mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e(LOG_TAG, "onChildAdded " + dataSnapshot.toString());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.e(LOG_TAG, "onChildChanged " + dataSnapshot.toString());

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e(LOG_TAG, "onChildRemoved " + dataSnapshot.toString());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.e(LOG_TAG, "onChildMoved " + dataSnapshot.toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG, "onCancelled " + databaseError.toString());

            }
        });*/


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        // Set up the login form.

        mLastNameView = findViewById(R.id.prompt_last_name);
        mNameView = findViewById(R.id.prompt_name);
        mMiddleNameView = findViewById(R.id.prompt_middle_name);
        mSeriesView = findViewById(R.id.prompt_series);
        mNumberView = findViewById(R.id.prompt_number);

        String fullName = preferences.getString("Param1", "");
        String[] param1 = fullName.split(" ");

        webView = findViewById(R.id.webView);
        mProgressView = findViewById(R.id.login_progress);
        mLoginFormView = findViewById(R.id.login_form);

		String mSeries = preferences.getString("Param2", "");
		String mNumber = preferences.getString("Param3", "");
		mSeriesView.setText(mSeries);
		mNumberView.setText(mNumber);

        if (param1.length>1) {
            mLastNameView.setText(param1[0]);
            mNameView.setText(param1[1]);
            mMiddleNameView.setText(param1[2]);
            startLoad(fullName, mSeries, mNumber, false);
        } else {
            mLoginFormView.setVisibility(View.VISIBLE);
        }


        Button mEmailSignInButton = (Button) findViewById(R.id.search);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLastNameView.clearFocus();
                mNameView.clearFocus();
                mMiddleNameView.clearFocus();
                mSeriesView.clearFocus();
                mNumberView.clearFocus();
                attemptLogin();

                /*String id = mDatabase.push().getKey();

                //creating an Artist Object
                Artist artist = new Artist("id", "name", "genre");

                User user = new User("id", "name", "genre");
                //Artist artist = new Artist(id, name, genre);

                //Saving the Artist
                mDatabase.child(id).setValue(artist);*/

            }
        });

        mReloginButton = (Button) findViewById(R.id.relogin);
		mReloginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				mLoginFormView.setVisibility(View.VISIBLE);
                mReloginButton.setVisibility(View.GONE);
				webView.setVisibility(View.GONE);
			}
		});
    }

    private void attemptLogin() {
        mLastNameView.setError(null);
        mNameView.setError(null);
        mMiddleNameView.setError(null);
        mSeriesView.setError(null);
        mNumberView.setError(null);

        String mLastName = mLastNameView.getText().toString();
        String mName = mNameView.getText().toString();
        String mMiddleName = mMiddleNameView.getText().toString();
        String mSeries = mSeriesView.getText().toString();
        String mNumber = mNumberView.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(mLastName) ) {
            mLastNameView.setError(getString(R.string.error_invalid_name));
            focusView = mLastNameView;
            cancel = true;
        } else if (TextUtils.isEmpty(mName) ) {
            mNameView.setError(getString(R.string.error_invalid_name));
            focusView = mNameView;
            cancel = true;
        } else if (TextUtils.isEmpty(mMiddleName) ) {
            mMiddleNameView.setError(getString(R.string.error_invalid_name));
            focusView = mMiddleNameView;
            cancel = true;
        } else if (TextUtils.isEmpty(mSeries) ) {
            mSeriesView.setError(getString(R.string.error_invalid_name));
            focusView = mSeriesView;
            cancel = true;
        } else if (TextUtils.isEmpty(mNumber) ) {
            mNumberView.setError(getString(R.string.error_invalid_name));
            focusView = mNumberView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            String Param1 = mLastName + " " + mName + " " + mMiddleName;
            startLoad(Param1, mSeries, mNumber, true);


            User user = new User(mLastName, mName, mMiddleName, mSeries, mNumber);
            //Artist artist = new Artist(id, name, genre);
            //Saving the Artist
            mDatabase.child(androidId).setValue(user);
        }
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null || inputManager == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void startLoad(String Param1, String mSeries, String mNumber, boolean b) {
        showProgress(true);

        hideKeyboard(this);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //InputMethodManager inputManager = (InputMethodManager) (LoginActivity) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        //inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        /*View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/

        if (b) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

            preferences.edit()
                    .putString("GuidControl", "2091")
                    .putString("Param1", Param1.toUpperCase(Locale.getDefault()))
                    .putString("Param2", mSeries.toUpperCase(Locale.getDefault()))
                    .putString("Param3", mNumber)
                    .apply();
        }


        new LoadJsonData(this).execute();
    }


    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1)/*.setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        })*/;

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0)/*.setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        })*/;
    }


    public void parseResponse(String result){
        showProgress(false);
        if (result != null) {
            mLoginFormView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.GONE);
            mReloginButton.setVisibility(View.VISIBLE);

            String sb = "<HTML><HEAD></HEAD><body><style>table{display: inline-block; height: auto; max-width: 100%;}</style>" +
                    result +
                    "</body></HTML>";

            webView.setVisibility(View.VISIBLE);
            webView.getSettings();
            webView.setBackgroundColor(Color.TRANSPARENT);
            webView.loadDataWithBaseURL("file:///android_asset/", sb, "text/html", "utf-8", null);
        }
    }
}

